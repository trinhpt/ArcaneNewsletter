const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const https = require("https");
const { url } = require("inspector");

const app = express();
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended:true}));

app.get("/", (req,res)=>{
  res.sendFile( __dirname + "/signup.html");
});

app.post("/failure", (req,res)=>{
  res.redirect("/");
});

app.post("/success", (req,res)=>{
  res.redirect("/");
});

app.post("/", (req,res)=>{
  let firstName = req.body.fName;
  let lastName = req.body.lName;
  let email = req.body.email;
  
  const data = {
    members: [
      {
        email_address: email,
        status: "subscribed", 
        merge_fields: {
          FNAME: firstName,
          LNAME: lastName
        }
      }
    ]
  }
  const jsonData = JSON.stringify(data);
  const url = "https://us2.api.mailchimp.com/3.0/lists/";
  const options = {
    method: "POST",
    auth: "zwonder:"
  }

  const request = https.request(url,options, (response)=>{
    if(response.statusCode === 200){
      res.sendFile( __dirname + "/succes.html");
    }
    else{
      res.sendFile(__dirname + "/failure.html")
    }
    response.on("data", function(data){
      console.log(JSON.parse(data));
    })
  });
  request.write(jsonData);
  request.end();
});

app.get('*', function(req, res){
  res.status(404).sendFile(__dirname + "/failure.html");
});

app.listen(3000, ()=>{
  console.log("server started");
})
